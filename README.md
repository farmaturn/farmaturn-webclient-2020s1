[![pipeline status](https://gitlab.com/farmaturn/farmaturn-webclient-2020s1/badges/master/pipeline.svg)](https://gitlab.com/farmaturn/farmaturn-webclient-2020s1/-/commits/master)
[![](https://jitpack.io/v/com.gitlab.farmaturn/farmaturn-model-2020s1.svg)](https://jitpack.io/#com.gitlab.farmaturn/farmaturn-model-2020s1)
[![coverage report](https://gitlab.com/farmaturn/farmaturn-webclient-2020s1/badges/master/coverage.svg)](https://farmaturn.gitlab.io/farmaturn-webclient-2020s1/code-coverage-reports)

# farmaturn-webclient-2020s1

- La idea de este proyecto es funcionar como vista para el ESB, para todas las configuraciones que necesite.
