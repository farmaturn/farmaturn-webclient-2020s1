/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package farmaturn.webclient.s1;

import org.junit.Test;
import static org.junit.Assert.*;

public class LibraryWebClientTest {
    @Test public void testSomeLibraryMethod() {
        LibraryWebClient classUnderTest = new LibraryWebClient();
        assertTrue("someLibraryMethod should return 'true'", classUnderTest.someLibraryMethod());
    }
}
